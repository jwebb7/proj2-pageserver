from flask import Flask, render_template
import os

app = Flask(__name__)

@app.route("/<path:name>")
def index(name):
    if any([x in name for x in ["//","~",".."]])or name[0] == "/":
        return forbidden(403)
    elif os.path.isfile("./templates/" + name):
        return render_template(name), 200
    elif os.path.isfile("./templates/" + name) is False:
        return page_not_found(404)

@app.errorhandler(403)
def forbidden(error):
	return render_template("403.html"), 403

@app.errorhandler(404)
def page_not_found(error):
	return render_template("404.html"), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
